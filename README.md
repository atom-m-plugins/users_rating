## Плагин для вывода топа пользователей для Atom-M CMS

## Установка:

1. Распаковать архив и переместить папку users_rating в plugins.
2. Для файлов plugins/users_rating/config.json и plugins/users_rating/template/list.html.twig выставить права 777.
3. Добавить метку в нужном месте шаблона:

```
{% if atm.plugin_users_rating %}
    {% include '@users_rating/list.html.twig' %}
{% endif %}
```

Автор стандартного плагина Drunya.
Обновил плагин Mishka, исправил ошибки modos189.